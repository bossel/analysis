### Following	parameters help the user that want to select the optional plots produced by POMEROL.
src_path /home/bossel/These/Analysis/src/rayleigh/	#Path to all the python source files
saving_graphs 0	# 0:False or 1:True, decide whether or not saving graphs for individual measures. Allows to gain time for almucantar or time dependant.
save_path /home/bossel/These/Documentation/Mes_articles/auroral_pola/sky_only/	# Path to the folder where you want to save your results (graphs, txt...)
save_name 20190307_m_sky_only_aero_1low_albedo	#Base for the name of your results files. It will be appended with a identifier (such as grd_map, sky_map, etc...)
# ###############################################################################	# "vcm-orm-ntl" (VIIRS Cloud Mask - Outlier Removed - Nighttime Lights) This product contains the "vcm-orm" average, with background (non-lights) set to zero.
flux_unit W
use_analytic false	# Should stay as "false". If "true", POMEROL will compute the integral along the line of sight using an automatic scipy integration function. It is only slightly faster, and do not allow for voume subdivisions.
azimuts all
elevations 45
location lagorge	#Accepted locations: lagorge, skibotn, stveran, nyalesund, vigan, mens or LON;LAT in degrees
start_datetime 20190116-02:44:00
end_datetime 20190116-04:00:00
instrument_opening_angle 1	# Half opening angle of the instrument (degrees)
instrument_area 20	# Area of Petit Cru in cm^2 (gets converted in km**2 in the code)
wavelength 630.0	#in nm
cst_Flux 0	# in nW
cst_DoLP 0	# in %
cst_AoLP 0	# in degrees
add_starlight 0	#Check if this is still working before use... Will add starlight as a function of the galactic latitude of the observation. Be sure to have the time and location right!
add_B_pola 0	#Check if this is still working before use... Add polarisation following the B field at the emission.
RS_min_altitude 0	# Minimum altitude of the scattering layer
RS_max_altitude 31	# Maximum altitude of the scattering layer (maximum 120km, because it's the maximum altitude of the atmospheric profiles)
Nb_points_along_los 100	# Number of bins along the line of sight between H_r_min and h_r_max
max_angle_discretization 0	# In degrees. If scattering volume solid angle seen from E is larger than that, cut the volume in pieces.
Atmospheric_profile atm_profiles/mlw.atm	#name of the atmospheric profile file to use http://eodg.atm.ox.ac.uk/RFM/atm/ (MIPAS model 2001 works for sure). Files stored in ./atm_profiles/
use_ozone 1	# 0 or 1. Will compute absorption along the paths due to ozone.
use_aerosol 0	# 0 or 1. Main flag to decide wether to use aerosol scattering/absorption or not.
aer_complexity 0	# 0: Use a simple C_ext(altitude) profile. 1: use a more complex model with tunable optical properties and density.
aer_model maritime	# manual: use the aerosols properties listed below. Or name of a pre configured model.
aer_name maritime_test	# Name to use for file handling of this particular properties. May erase previous files if same name is used.
aer_nr 1.6	# Real optical index
aer_ni -0.010	# Imaginary optical index <0
aer_rn0 1	# Number of particles for the Mie code to run (keep it to 1)
aer_rmg 0.001	# (micrometers) Peak size for aerosols size distribution fct (see mie/pmie.f for the expression)
aer_ln_sigma 0.657	# Parameter for aerosols size distribution (see mie/pmie.f for the expression)
aer_r_min 0.001	# Minimum aerosol size (micrometers)
aer_r_max 50	# Maximum aerosol size (micrometers)
total_OD 1.075	# Total vertical optical depth of the aerosols (from ground to space). If used (!=0), the concentration and extinction will be scaled to match the specified value.
aer_Hn 0.440	# Scale height (km)
aer_n0 300	# Surface value (cm-3)
aer_nBK 30	# Background value (cm-3)
aer_ext_profile_name mar	#urban (urb), continental (con), maritime (mar) from WCP-12 (World Climate Programme 1980)
aer_single_scatter_albedo 0.9
aer_phase_fct_asym_g 0.7	#[-1; 1], g>0 -> front scatter dominant. g<0 -> back scatter dominant
sky_mode none	image uniform_100 movie image uniform_100 1_band_e45 none starlight_20190116-030000 uniform_0 1_moving_EW_band movie image spot_I100_a90_e45 movie 1_band_e45 spot_I100_a90_e90 image uniform_219
direct_mode add
emission_altitude 110	# Altitude of the emission in km.
Nb_emission_points 1000	# Number of bins to compute. Will downgrade the initial picture to match this pixel count.
Nb_emission_maps 1	# Number of emission maps to compute. Use it for time changing synthetic emissions (sky_mode == X_moving_EW_band, but not for a movie). Should be put to 1 when not varying !
#If loading	the sky from a picture: sky_mode == image or movie
sky_path /home/bossel/These/Analysis/data/allsky_camera/tid.uio.no/plasma/aurora/	# constant path to the images
#sky_file skn4/5577/2019/20190307/ut20/	#path after sky_path where the pictures are stored
sky_file skn4/5577/2019/20190308/ut01/skn4_20190308_012908_5577_cal.png	#clean green rotation 20190307
#movie_file_numbers ::	#Indicate Start:End:Step for the file list. Files are read in chronological order.
sky_wavelength 557.7	# Check if this works. To be able to pass from Rayleigh to nW.
use_alt_map 0	# 0 or 1, if you want to compute terrain occultations and environmental geometry with real terrain elevations.
alt_map_path /home/bossel/These/Analysis/data/elevation/	#Path where the elevations data are stored
alt_map_radius 100	#radius of the map in km. Can be different from the ground emission map
alt_map_N_bins_max 0	#Maximum number of bins to use in the map.
alt_map_resolution 0.5	.031 # Resolution of the map you want to achieve in km. Minimum 1 arcsec = 31m = 0.031 km. The map will be downgraded to match the resolution.
ground_mode point_I100_a180_d5	uniform_I100 point_I100_a180_d5 none none image uniform_I100 gaussian_I100_w1_a45_d10
shader_mode none	ring_0 out_dist_m2.5_M3 half_a0 out_dist_m90_M100 half_a0 none random_01 half_a0
ground_emission_radius 100	# in km. Can be != from the altitude map
ground_N_bins_max 1000	# the map will be downgraded to match this pixel count.
ground_albedo altitude_0.2_0.2_0.5_0.5_0.8	constant_0.5
ground_path /home/bossel/These/Analysis/data/Night_pollution/
ground_file nighttime_light_files/SVDNB_npp_20160101-20161231_75N060W_vcm-orm-ntl_v10_c201807311200.avg_rade9.tif
show_ground_albedo 0	# Produce a map of the ground emissions with the albedo layers as contour plots (only if ground_albedo is set to altitude_...).
show_sky_cube 0	# Produce a plot with all sky maps used in the simulation. Usefull for time-evolving setups and movie sky mode.
show_cross_section 0	# Produce a plot of aerosol cross sections.
show_atmosphere 0	# Produce a plot of temperature, pression, and O3 profile of the atmosphere
